from django.db import models

from apps.generales.models import ModeloBase


class Seguimiento(ModeloBase):
    SELECCIONAR = 'Seleccione una opción'
    NO_CONTACTADO = 'No fuí contactado nunca por el profesional encargado del seguimiento'
    NO_SEGUIDO = 'El profesional no realiza el seguimiento todos los días'
    CONFORME = 'Estoy conforme con el seguimiento realizado por el profesional'
    NO_CONFORME = 'No estoy conforme con el seguimiento realizado por el profesional'
    CALIFICACION =[
        (SELECCIONAR, 'Seleccione una opción'),
        (NO_CONTACTADO, 'No fuí contactado nunca por el profesional encargado del seguimiento'),
        (NO_SEGUIDO, 'El profesional no realiza el seguimiento todos los días'),
        (CONFORME, 'Estoy conforme con el seguimiento realizado por el profesional'),
        (NO_CONFORME, 'No estoy conforme con el seguimiento realizado por el profesional')
    ]
    dni = models.CharField(max_length=12)
    numero_afiliado = models.CharField(max_length=20)
    nombre = models.CharField(max_length=120)
    calificacion = models.CharField(max_length=75, choices=CALIFICACION, default=SELECCIONAR)
    opinion = models.TextField()
    celular = models.CharField(max_length=25)
    email = models.CharField(max_length=120)
    
    def __str__(self):
        return '{} - {}'.format(self.nombre, self.dni)
    
    class Meta:
        verbose_name_plural = "Seguimiento COVID"
