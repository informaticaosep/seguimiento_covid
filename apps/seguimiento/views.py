from django.shortcuts import render

from django.urls import reverse_lazy

# Vistas genéricas
from django.views import generic

# Mensajes
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin

# Mixins
from django.contrib.auth.mixins import LoginRequiredMixin

import django_excel as excel

from datetime import datetime

# Formularios
from .forms import SeguimientoForm

# Modelos
from .models import Seguimiento


# Publico
class SeguimientoNew(SuccessMessageMixin, generic.CreateView):
    model = Seguimiento
    template_name = 'public/seguimiento_form.html'
    context_object_name = 'obj'
    form_class = SeguimientoForm
    success_message = "Sus datos fueron registrados exitosamente"
    success_url = reverse_lazy("seguimiento:datos_recibidos")


class ReceivedData(generic.TemplateView):
    template_name = 'public/datos_recibidos.html'


# Restringido
class PatientList(LoginRequiredMixin, generic.ListView):
    model = Seguimiento
    template_name = 'dashboard/listado_pacientes.html'
    context_object_name = 'obj'
    login_url = 'usuarios:inicio'


def download_patient_list(self):
    # https://www.comoinstalarlinux.com/como-exportar-a-un-excel-con-python-y-django/
    export = []

    export.append(['DNI', 'N° de Afiliado', 'Nombre', 'Fecha de Carga', 'Calificación', 'Opinión', 'Celular', 'Email'])
    results = Seguimiento.objects.all()

    for result in results:
        export.append([
            result.dni,
            result.numero_afiliado,
            result.nombre,
            result.fecha_creacion,
            result.calificacion,
            result.opinion,
            result.celular,
            result.email,
        ])

    today = datetime.now()
    strToday = today.strftime("%d%m%Y")

    # se transforma el array a una hoja de calculo en memoria
    sheet = excel.pe.Sheet(export)

    # se devuelve como "Response" el archivo para que se pueda "guardar"
    # en el navegador, es decir como hacer un "Download"
    # return excel.make_response(sheet, "csv", file_name="results-" + strToday + ".csv")
    # return excel.make_response(sheet, "ods", file_name="results-" + strToday + ".ods")
    return excel.make_response(sheet, "xlsx", file_name="listado_pacientes_covid_osep-" + strToday + ".xlsx")
