from django.urls import path

# Vistas
from .views import SeguimientoNew, ReceivedData, download_patient_list, PatientList

urlpatterns =[
    # Public
    path('', SeguimientoNew.as_view(), name='seguimiento_new'),
    path('datos_recibidos/', ReceivedData.as_view(), name='datos_recibidos'),
    # Dashboard
    path('listado_pacientes/', PatientList.as_view(), name='listado_pacientes'),
    path('descargar_listado_pacientes', download_patient_list, name='descargar_listado_pacientes'),
]