from django import forms

# Modelos
from .models import Seguimiento



class SeguimientoForm(forms.ModelForm):
    class Meta:
        model = Seguimiento
        fields = ['dni', 'numero_afiliado', 'nombre', 'calificacion', 'opinion', 'celular', 'email']
        labels = {
            'dni': "DNI",
            'numero_afiliado': "Número de Afiliado",
            'nombre': "Nombres y Apellido",
            'calificacion': "Calificación",
            'opinion': "Opinion",
            'celular': "Celular",
            'email': "Email",
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
