from django.db import models

""" Importo el modelo de usuario. """
from django.contrib.auth.models import User

# Modelos
from apps.generales.models import ModeloBase


# Perfiles
class Perfil(ModeloBase):
    """ Registro cada perfil de usuario. """
    usuario = models.OneToOneField(User, on_delete=models.CASCADE)
    nivel = models.CharField(max_length=70, null=False, blank=False)

    def __str__(self):
        return "Usuario: {} / Nivel: {}".format(self.usuario, self.nivel)

    class Meta:
        verbose_name_plural = "Perfiles"
