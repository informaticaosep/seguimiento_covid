from django.urls import path

""" Importo las vistas de login/logout de Django. """
from django.contrib.auth import views as auth_views

# Vistas
from .views import ingreso, Inicio


urlpatterns =[
    # Login, Logout
    path('login/', ingreso, name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='login.html'), name='logout'),
    # Página de Inicio
    path('inicio', Inicio.as_view(), name='inicio'),
]