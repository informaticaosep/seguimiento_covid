from django.contrib import admin

# Modelos
from apps.usuarios.models import Perfil


class PerfilAdmin(admin.ModelAdmin):
    list_display = ('id', 'usuario', 'nivel')
    list_display_links = ('id', 'usuario')
    search_fields = ('nivel', 'usuario__username')


admin.site.register(Perfil, PerfilAdmin)
