from django import forms

""" Importo para poder usar en el formulario de ingreso. """
from django.utils.translation import gettext_lazy as _

from captcha.fields import CaptchaField

# Modelos
from django.contrib.auth.models import User


# Ingreso
class IngresoForm(forms.Form):
    username = forms.CharField(max_length=40)
    password = forms.CharField(label='Password', widget=forms.PasswordInput)
    captcha = CaptchaField()

    class Meta:
        model = User
        fields = ['username', 'password']
        label = {
            'username': 'Nombre de usuario',
            'password': 'Contraseña'
        }

    def __init__(self, *args, **kwars):
        super().__init__(*args, **kwars)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
        self.fields['username'].widget.attrs['placeholder'] = 'Usuario'
        self.fields['password'].widget.attrs['placeholder'] = 'Contraseña'

    error_messages = {
        'invalid_login': _(
            "Please enter a correct %(username)s and password. Note that both "
            "fields may be case-sensitive."
        ),
        'inactive': _("This account is inactive."),
    }
