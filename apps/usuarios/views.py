from django.shortcuts import render, redirect

from django.contrib import messages
from django.contrib.auth import authenticate, login

from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic

# Formularios
from .forms import IngresoForm


def ingreso(request):
    template = "login.html"

    if request.method == 'POST':
        form = IngresoForm(request.POST)

        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('usuarios:inicio')
            else:
                messages.error(request, 'Usuario o contraseña equivocados.')
                return redirect('usuarios:login')
    else:
        form = IngresoForm()

    context = {
             'form': form
         }

    return render(request, template, context)


class Inicio(LoginRequiredMixin, generic.TemplateView):
    template_name = 'inicio.html'
    login_url = 'usuarios:login'

