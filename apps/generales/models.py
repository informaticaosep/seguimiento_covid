from django.db import models


class ModeloBase(models.Model):
    activo = models.BooleanField(default=True)
    fecha_ultimo_movimiento = models.DateField(auto_now=True)
    fecha_creacion = models.DateField(auto_now_add=True)

    class Meta:
        abstract = True